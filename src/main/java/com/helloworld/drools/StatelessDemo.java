package com.helloworld.drools;

import com.helloworld.drools.beans.Applicant;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;

/**
 * Created with IntelliJ IDEA.
 * User: Bruce jia (chen-wei.jia@helloworld.com)
 * Date: 14-3-26
 * Time: 下午2:17
 * To change this template use File | Settings | File Templates.
 */
public class StatelessDemo {
    public static void main(String[] args) {
        KieContainer kContainer = KieServices.Factory.get().getKieClasspathContainer();

        StatelessKieSession kieSession = kContainer.newStatelessKieSession("SLKS");

        System.out.println("Get kieSession: " + kieSession);

        Applicant applicant = new Applicant( "Mr John Smith", 20 );

        System.out.println("Before execute: "+applicant.isValid());

        kieSession.execute(applicant);

        if(applicant.isValid()){
            //TODO
        }

        System.out.println("After execute: "+applicant.isValid());

    }
}
