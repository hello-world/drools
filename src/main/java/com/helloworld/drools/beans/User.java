package com.helloworld.drools.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Bruce jia (chen-wei.jia@helloworld.com)
 * Date: 14-5-12
 * Time: 下午2:43
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private int money; // 手中的钱
    private int kp; // 空瓶数
    private int totals; // 喝掉的瓶数

    public int getMoney() {
        return money;
    }

    public User setMoney(int money) {
        this.money = money;
        return this;
    }

    public int getKp() {
        return kp;
    }

    public void setKp(int kp) {
        this.kp = kp;
    }

    public int getTotals() {
        return totals;
    }

    public void setTotals(int totals) {
        this.totals = totals;
    }

}
