package com.helloworld.drools.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Bruce jia (chen-wei.jia@helloworld.com)
 * Date: 14-3-26
 * Time: 下午12:41
 * To change this template use File | Settings | File Templates.
 */
public class Applicant {

    private String name;

    private int age;

    private boolean valid;

    public Applicant(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

}
