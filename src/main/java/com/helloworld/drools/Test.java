package com.helloworld.drools;

import com.helloworld.drools.beans.User;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * Created with IntelliJ IDEA.
 * User: Bruce jia (chen-wei.jia@helloworld.com)
 * Date: 14-5-12
 * Time: 下午2:44
 * To change this template use File | Settings | File Templates.
 */
public class Test {
    public static void main(String[] args) throws Exception {
        KieSession kieSession = getKieSessionInstance();

        //Set money
        kieSession.insert(new User().setMoney(20));

        kieSession.fireAllRules();
        kieSession.dispose();
    }

    public static KieSession getKieSessionInstance(){
        KieContainer kContainer = KieServices.Factory.get().getKieClasspathContainer();

        KieSession kieSession = kContainer.newKieSession("KS1");

        System.out.println("Get kieSession: " + kieSession);

        return kieSession;
    }
}
