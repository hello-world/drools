package com.helloworld.drools;

import com.helloworld.drools.beans.Applicant;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * Created with IntelliJ IDEA.
 * User: Bruce jia (chen-wei.jia@helloworld.com)
 * Date: 14-3-26
 * Time: 下午4:38
 * To change this template use File | Settings | File Templates.
 */
public class StatefulDemo {
    public static void main(String[] args) {
        KieContainer kContainer = KieServices.Factory.get().getKieClasspathContainer();

        KieSession kieSession = kContainer.newKieSession("KS1");

        System.out.println("Get kieSession: " + kieSession);

        Applicant applicant = new Applicant( "Mr John Smith", 20 );

        System.out.println("Before execute: "+applicant.isValid());

        kieSession.insert(applicant);
        kieSession.fireAllRules();

        if(applicant.isValid()){
            //TODO
        }

        System.out.println("After execute: "+applicant.isValid());

    }
}
